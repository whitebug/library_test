import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:library_test/blocs/blocs.dart';
import 'package:library_test/models/book_model.dart';
import 'package:library_test/repository/repository.dart';

class BookBloc extends Bloc<BookEvent, BookState> {
  final LibraryRepository libraryRepository;

  BookBloc({@required this.libraryRepository});

  @override
  BookState get initialState => BooksLoadingState();

  @override
  Stream<BookState> mapEventToState(
    BookEvent event,
  ) async* {
    if (event is LoadingBooksEvent) {
      yield* _mapLoadingBooksEventToState();
    } else if (event is BooksLoadEvent) {
      yield* _mapBooksLoadedEventToState();
    } else if (event is CreateBookEvent) {
      yield* _mapCreateBookEventToState(event);
    } else if (event is UpdateBookEvent) {
      yield* _mapUpdateBookEventToState(event);
    } else if (event is DeleteBookEvent) {
      yield* _mapDeleteBookEventToState(event);
    }
  }

  Stream<BookState> _mapLoadingBooksEventToState() async* {
    yield BooksLoadingState();
  }

  Stream<BookState> _mapBooksLoadedEventToState() async* {
    List<BookModel> books = await libraryRepository.getAllBooks();
    yield BooksLoadedState(books: books);
  }

  Stream<BookState> _mapCreateBookEventToState(CreateBookEvent event) async* {
    try {
      await libraryRepository.createBook(event.book);
    } catch(error) {
      yield BooksCreationErrorState(error: error);
    }
  }

  Stream<BookState> _mapUpdateBookEventToState(UpdateBookEvent event) async* {
    try {
      await libraryRepository.updateBook(event.book);
    } catch(error) {
      yield BookUpdatedErrorState(error: error);
    }
  }

  Stream<BookState> _mapDeleteBookEventToState(DeleteBookEvent event) async* {
    try {
      await libraryRepository.deleteBook(event.id);
    } catch(error) {
      yield BooksLoadedErrorState(error: error);
    }
  }
}
