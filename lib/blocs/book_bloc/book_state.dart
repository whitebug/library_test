import 'package:equatable/equatable.dart';
import 'package:library_test/models/models.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BookState extends Equatable {
  BookState([List props = const <dynamic>[]]) : super(props);
}

class BooksLoadingState extends BookState {
  @override
  String toString() => 'BooksLoadingState';
}

class BooksLoadedState extends BookState {
  final List<BookModel> books;

  BooksLoadedState({@required this.books}) : super([books]);

  @override
  String toString() => 'BooksLoadedState {first book title: ${books[0].title}';
}

class BooksCreationErrorState extends BookState {
  final dynamic error;
  BooksCreationErrorState({@required this.error}) : super([error]);

  @override
  String toString() => 'BooksCreationErrorState {error: ${error.toString()}';
}

class BooksLoadedErrorState extends BookState {
  final dynamic error;
  BooksLoadedErrorState({@required this.error}) : super([error]);

  @override
  String toString() => 'BooksLoadedErrorState {error: ${error.toString()}';
}

class BookUpdatedErrorState extends BookState {
  final dynamic error;

  BookUpdatedErrorState({@required this.error}) : super([error]);

  @override
  String toString() => 'BookUpdatedErrorState {error: ${error.toString()}';
}
