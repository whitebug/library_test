import 'package:equatable/equatable.dart';
import 'package:library_test/models/book_model.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BookEvent extends Equatable {
  BookEvent([List props = const <dynamic>[]]) : super(props);
}

class LoadingBooksEvent extends BookEvent {
  @override
  String toString() => 'LoadingBooksEvent';
}

class BooksLoadEvent extends BookEvent {
  @override
  String toString() => 'LoadBooksEvent';
}

class CreateBookEvent extends BookEvent {
  final BookModel book;

  CreateBookEvent({@required this.book}) : super([book]);

  @override
  String toString() => 'CreateBookEvent {book: ${book.title}';
}

class UpdateBookEvent extends BookEvent {
  final BookModel book;

  UpdateBookEvent({@required this.book}) : super([book]);

  @override
  String toString() => 'UpdateBookEvent {book: ${book.title}';
}

class DeleteBookEvent extends BookEvent {
  final int id;

  DeleteBookEvent({@required this.id}) : super([id]);

  @override
  String toString() => 'DeleteBookEvent {book id to delete: $id}';
}
