import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:library_test/blocs/blocs.dart';
import 'package:library_test/models/models.dart';

class EditPage extends StatelessWidget {
  final BookModel book;

  EditPage({Key key, @required this.book})
      : super(key: key);

  final _titleController = TextEditingController();
  final _authorController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _titleController.text = book.title;
    _authorController.text = book.author;
    final _bookBloc = BlocProvider.of<BookBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Редактирование книги'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              TextField(
                controller: _titleController,
                decoration: InputDecoration(labelText: 'Название книги'),
              ),
              TextField(
                controller: _authorController,
                decoration: InputDecoration(
                  labelText: 'Автор книги',
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              RaisedButton(
                child: Text('Сохранить изменения'),
                onPressed: () {
                  BookModel updatedBook = BookModel(
                    id: book.id,
                    title: _titleController.text,
                    author: _authorController.text,
                  );
                  _bookBloc.dispatch(UpdateBookEvent(book: updatedBook));
                  _bookBloc.dispatch(BooksLoadEvent());
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
