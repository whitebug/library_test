import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:library_test/blocs/blocs.dart';
import 'package:library_test/models/book_model.dart';

class AddBookPage extends StatelessWidget {
  final _titleController = TextEditingController();
  final _authorController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final _bookBloc = BlocProvider.of<BookBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Добавить книгу'
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              TextField(
                controller: _titleController,
                decoration: InputDecoration(
                    labelText: 'Название книги'
                ),
              ),
              TextField(
                controller: _authorController,
                decoration: InputDecoration(
                    labelText: 'Автор книги',
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              RaisedButton(
                child: Text(
                  'Добавить книгу'
                ),
                onPressed: () {
                  BookModel book = BookModel(
                    title: _titleController.text,
                    author: _authorController.text,
                  );
                  _bookBloc.dispatch(CreateBookEvent(book: book));
                  _bookBloc.dispatch(BooksLoadEvent());
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}