import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:library_test/blocs/blocs.dart';
import 'package:library_test/models/book_model.dart';
import 'package:library_test/screens/screens.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  BookBloc _bookBloc;
  List<BookModel> books;

  @override
  void initState() {
    _bookBloc = BlocProvider.of<BookBloc>(context);
    _bookBloc.dispatch(BooksLoadEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Books list'),
      ),
      body: BlocBuilder(
        bloc: _bookBloc,
        builder: (BuildContext context, BookState state) {
          if(state is BooksLoadedState){
            books = state.books;
            if(books.length>0){
              return ListView.builder(
                  itemCount: books.length,
                  itemBuilder: (BuildContext context, index) {
                    BookModel book = books[index];
                    return Card(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              flex: 2,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        book.title,
                                        maxLines: 2,
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        book.author,
                                        maxLines: 2,
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Flexible(
                              flex: 1,
                              child: Column(
                                children: <Widget>[
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: (){
                                      Navigator.of(context).push(
                                        MaterialPageRoute(builder: (_) {
                                          return EditPage(
                                            book: book,
                                          );
                                        }),
                                      );
                                    },
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.delete),
                                    onPressed: (){
                                      _bookBloc.dispatch(DeleteBookEvent(id: book.id));
                                      _bookBloc.dispatch(BooksLoadEvent());
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
              );
            }else{
              return Container(
                child: Center(
                  child: Text(
                    'Книг нет'
                  ),
                ),
              );
            }
          }else{
            return Container();
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (_) {
              return AddBookPage();
            }),
          );
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }

  @override
  void dispose() {
    _bookBloc.dispose();
    super.dispose();
  }
}
