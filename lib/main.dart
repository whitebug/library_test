import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:library_test/blocs/blocs.dart';
import 'package:library_test/repository/library_repository.dart';
import 'package:library_test/screens/screens.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final BookBloc _bookBloc = BookBloc(libraryRepository: LibraryRepository());
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider>[
        BlocProvider<BookBloc>(
          builder: (BuildContext context) => _bookBloc,
        )
      ],
      child: MaterialApp(
        title: 'Library Test',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage(),
      ),
    );
  }
}


