import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';


class BookModel extends Equatable {
  final int id;
  final String title;
  final String author;

  BookModel({this.id, @required this.title, @required this.author})
      : super([id, title, author]);

  BookModel copyWith({
    int id,
    String title,
    String author,
  }) {
    return BookModel(
      id: id ?? this.id,
      title: title ?? this.title,
      author: author ?? this.author,
    );
  }

  @override
  String toString() {
    return 'BookModel { id: $id, title: $title, author: $author }';
  }

  Map<String, dynamic> toDatabaseJson() => {
        "id": this.id,
        "title": this.title,
        "author": this.author,
      };

  factory BookModel.fromDatabaseJson(Map<String, dynamic> data) => BookModel(
        id: data['id'],
        title: data['title'],
        author: data['author'],
      );

  static BookModel fromJson(Map<String, Object> json) {
    return BookModel(
      id: json["id"] as int,
      title: json["title"] as String,
      author: json["author"] as String,
    );
  }
}
