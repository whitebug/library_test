import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

final bookTable = 'books';

class LibraryDB {
  static final LibraryDB libraryDatabase = LibraryDB();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await createDatabase();
    return _database;
  }

  createDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    String path = join(documentsDirectory.path, "Library.db");

    var database = await openDatabase(path,
        version: 1, onCreate: initDB, onUpgrade: onUpgrade);
    return database;
  }

  void onUpgrade(Database database, int oldVersion, int newVersion) async{
    if ((newVersion > oldVersion)) {
    }
  }

  void initDB(Database database, int version) async {
    await database.execute('''
      CREATE TABLE $bookTable(
        id INTEGER PRIMARY KEY,
        title VARCHAR(191),
        author VARCHAR(191)
      )
      ''');
  }
}
