import 'dart:async';
import 'dart:core';
import 'package:library_test/models/models.dart';
import 'repository.dart';

class LibraryRepository {
  final libraryDAO = LibraryDAO();

  ///Get all books
  Future<List<BookModel>> getAllBooks() async {
    return await libraryDAO.getBooks();
  }

  Future createBook(BookModel bookModel) => libraryDAO.createBook(bookModel);

  Future updateBook(BookModel bookModel) => libraryDAO.updateBookDB(bookModel);

  Future deleteBook(int id) => libraryDAO.deleteBookDB(id);
}
