import 'dart:async';
import 'package:library_test/models/models.dart';
import 'repository.dart';

class LibraryDAO {
  final projectDatabase = LibraryDB.libraryDatabase;

  Future<int> createBook(BookModel book) async {
    final db = await projectDatabase.database;
    var result = db.insert(bookTable, book.toDatabaseJson());
    return result;
  }

  Future<List<BookModel>> getBooks(
      {List<String> columns, String query}) async {
    final db = await projectDatabase.database;

    List<Map<String, dynamic>> result;
    if (query != null) {
      if (query.isNotEmpty) {
        result = await db.query(bookTable,
          columns: columns,
          where: 'title LIKE ?',
          whereArgs: ["%$query%"],
        );
      }
    } else {
      result = await db.query(bookTable, columns: columns);
    }

    List<BookModel> books = result.isNotEmpty
        ? result.map((item) => BookModel.fromDatabaseJson(item)).toList()
        : [];
    return books;
  }

  ///Get books by ids list (using with possible web client)
  Future<List<BookModel>> getBookByIds(List<int> ids) async {
    final db = await projectDatabase.database;
    var idsString = ids.map((it) => '"$it"').join(',');
    var result = await db
        .rawQuery('SELECT * FROM $bookTable WHERE id IN ($idsString)');
    List<BookModel> book = result.isNotEmpty
        ? result.map((item) => BookModel.fromDatabaseJson(item)).toList()
        : [];
    return book;
  }

  Future<int> updateBookDB(BookModel book) async {
    final db = await projectDatabase.database;
    var result = await db.update(bookTable, book.toDatabaseJson(),
        where: "id = ?",
        whereArgs: [book.id]
    );
    return result;
  }

  Future<int> deleteBookDB(int id) async {
    final db = await projectDatabase.database;

    var result = await db.delete(bookTable,
        where: "id = ?",
        whereArgs: [id]
    );
    return result;
  }
}